import Page from './page';

class HomePage extends Page {

    get strangerNum() { return $('#xp__guests__toggle'); }
    get childNum() { return $('#group_children'); }
    set childNum(n) {
        this.childNum.selectByValue(n);
    }
    get childAges() { return $$('.sb-group__children__field > select') }

    open() {
        super.open('/');
    }

}

export default new HomePage();
