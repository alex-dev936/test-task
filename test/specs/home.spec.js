import HomePage from '../pageobjects/home.page';
import assert from 'assert';

describe('age form', () => {
    it('should have the number of children age inputs equal to the number of children', () => {
        const CHILD_NUM = 3;

        HomePage.open();
        HomePage.strangerNum.click();
        HomePage.childNum = CHILD_NUM;

        assert.equal(HomePage.childAges.length, CHILD_NUM)
    });
});